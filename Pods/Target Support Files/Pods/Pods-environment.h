
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// FXForms
#define COCOAPODS_POD_AVAILABLE_FXForms
#define COCOAPODS_VERSION_MAJOR_FXForms 1
#define COCOAPODS_VERSION_MINOR_FXForms 1
#define COCOAPODS_VERSION_PATCH_FXForms 6

// Toast
#define COCOAPODS_POD_AVAILABLE_Toast
#define COCOAPODS_VERSION_MAJOR_Toast 2
#define COCOAPODS_VERSION_MINOR_Toast 4
#define COCOAPODS_VERSION_PATCH_Toast 0

