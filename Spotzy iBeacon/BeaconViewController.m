//
//  ViewController.m
//  Spotzy iBeacon
//
//  Created by Mohamed Nassar on 26/11/2014.
//  Copyright (c) 2014 Mohamed Nassar. All rights reserved.
//

#import "BeaconViewController.h"


#define IBEACON_UUID @"9C0197EE-1B84-46EF-BC9C-3BFD2767DA05"
#define IBEACON_UUID2 @"B6FF08A0-0AF5-441C-9A13-AD5DE735D4A0"
#define IBEACON_UUID3 @"2F1C20BA-1B11-4E71-8FF6-168BC340688C"
#define IBEACON_UUID4 @"65E8D547-2FAE-43B5-9EF7-5D781958E4BB"
#define IBEACON_UUID5 @"C330E0D1-133B-4618-9ACA-EE5611123441"

#define IBEACON_STORE_NAME_1 @"Porsche"
#define IBEACON_STORE_NAME_2 @"BMW"
#define IBEACON_STORE_NAME_3 @"Burberyy"
#define IBEACON_STORE_NAME_4 @"Ferarri"
#define IBEACON_STORE_NAME_5 @"Lamborghini"


#define BEACON_IDENTIFIER @"com.mnassar.spotzybeacon"

@interface BeaconViewController ()

@end

@implementation BeaconViewController

@synthesize uuidTextField, beaconMajorValueTextField, beaconMinorValueTextField;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    uuidTextField.text = IBEACON_STORE_NAME_1;
    
    // Create the peripheral manager.
    peripheralManager = [[CBPeripheralManager alloc] initWithDelegate:self queue:nil options:nil];
    
    uuidArray = [[NSArray alloc] initWithObjects:IBEACON_UUID, IBEACON_UUID2, IBEACON_UUID3, IBEACON_UUID4, IBEACON_UUID5, nil];
    storeNamesArray = [[NSArray alloc] initWithObjects:IBEACON_STORE_NAME_1, IBEACON_STORE_NAME_2, IBEACON_STORE_NAME_3, IBEACON_STORE_NAME_4, IBEACON_STORE_NAME_5, nil];
    
    chosenUUID = [uuidArray objectAtIndex:0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)startBeaconBroadcasting:(UIButton *)sender {
    
    if(peripheralManager.isAdvertising) {
        NSLog(@"Already advertising");
    }
    
    else {
        
        CLBeaconMajorValue beaconMajor = [self.beaconMajorValueTextField.text integerValue];
        CLBeaconMinorValue beaconMinor = [self.beaconMinorValueTextField.text integerValue];
        
        [self broadcastBeaconWithUUID:chosenUUID andMajor:beaconMajor andMinor:beaconMinor];
        
    }
}

- (void) broadcastBeaconWithUUID: (NSString *) uuid andMajor: (CLBeaconMajorValue) major andMinor: (CLBeaconMinorValue) minor {
    
    
    NSUUID *beaconUUID = [[NSUUID alloc] initWithUUIDString:uuid];
//    CLBeaconMajorValue beaconMajor = [self.beaconMajorValueTextField.text integerValue];
//    CLBeaconMinorValue beaconMinor = [self.beaconMinorValueTextField.text integerValue];
    
    CLBeaconRegion *beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:beaconUUID major:major minor:minor identifier:BEACON_IDENTIFIER];
    
    // Create a dictionary of advertisement data.
    NSDictionary *beaconPeripheralData = [beaconRegion peripheralDataWithMeasuredPower:nil];
    
    NSLog(@"%ld", peripheralManager.state);
    
    // Start advertising your beacon's data.
    [peripheralManager startAdvertising:beaconPeripheralData];
    
    NSLog(@"%ld", peripheralManager.state);
    
}

- (IBAction)chooseUUIDButtonTapped:(UIButton *)sender {
    
    [UIView animateWithDuration:0.3 animations:^{
       
        pickerContainerView.frame = CGRectMake(0, [[UIScreen mainScreen] bounds].size.height - pickerContainerView.frame.size.height, [[UIScreen mainScreen] bounds].size.width, pickerContainerView.frame.size.height);
        
    }];
    
}

- (IBAction)doneButtonPickerViewTapped:(UIButton *)sender {
    
    [UIView animateWithDuration:0.3 animations:^{
        
        pickerContainerView.frame = CGRectMake(0, [[UIScreen mainScreen] bounds].size.height, [[UIScreen mainScreen] bounds].size.width, pickerContainerView.frame.size.height);
        
    }];
    
    chosenUUID = [uuidArray objectAtIndex:[_pickerView selectedRowInComponent:0]];
    
    self.uuidTextField.text = [storeNamesArray objectAtIndex:[_pickerView selectedRowInComponent:0]];
}


#pragma mark - Text Field Delegate

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
}

#pragma mark - CBPeriphiralManager delegate

- (void)peripheralManagerDidUpdateState:(CBPeripheralManager *)peripheral {
    NSLog(@"%d", peripheralManager.state);
}

- (void)peripheralManagerDidStartAdvertising:(CBPeripheralManager *)peripheral error:(NSError *)error {
    
    NSLog(@"Started advertisting beacon");
    
    self.beaconStatusSwitch.on = YES;
    self.startBeaconButton.enabled = NO;
    
    
}


#pragma mark - PickerView delegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    
    return 1;
    
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return uuidArray.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    return [storeNamesArray objectAtIndex:row];
    
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
}


@end
