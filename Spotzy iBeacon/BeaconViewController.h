//
//  ViewController.h
//  Spotzy iBeacon
//
//  Created by Mohamed Nassar on 26/11/2014.
//  Copyright (c) 2014 Mohamed Nassar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import <CoreLocation/CoreLocation.h>

@interface BeaconViewController : UIViewController <UITextFieldDelegate, CBPeripheralManagerDelegate, UIPickerViewDataSource, UIPickerViewDelegate> {
    
    CBPeripheralManager *peripheralManager;
    
    NSArray *uuidArray;
    NSArray *storeNamesArray;
    __weak IBOutlet UIView *pickerContainerView;
    
    NSString *chosenUUID;
}

@property (weak, nonatomic) IBOutlet UITextField *uuidTextField;
@property (weak, nonatomic) IBOutlet UITextField *beaconMajorValueTextField;
@property (weak, nonatomic) IBOutlet UITextField *beaconMinorValueTextField;
@property (weak, nonatomic) IBOutlet UISwitch *beaconStatusSwitch;
@property (weak, nonatomic) IBOutlet UIButton *startBeaconButton;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;


- (IBAction)startBeaconBroadcasting:(UIButton *)sender;
- (IBAction)chooseUUIDButtonTapped:(UIButton *)sender;
- (IBAction)doneButtonPickerViewTapped:(UIButton *)sender;


@end

