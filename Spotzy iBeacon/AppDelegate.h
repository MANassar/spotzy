//
//  AppDelegate.h
//  Spotzy iBeacon
//
//  Created by Mohamed Nassar on 26/11/2014.
//  Copyright (c) 2014 Mohamed Nassar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

