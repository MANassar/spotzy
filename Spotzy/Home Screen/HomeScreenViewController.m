//
//  HomeScreenViewController.m
//  Spotzy
//
//  Created by Mohamed Nassar on 29/11/2014.
//  Copyright (c) 2014 Mohamed Nassar. All rights reserved.
//

#import "HomeScreenViewController.h"
#import "StoreManager.h"


#define IBEACON_UUID @"9C0197EE-1B84-46EF-BC9C-3BFD2767DA05"
#define IBEACON_UUID2 @"B6FF08A0-0AF5-441C-9A13-AD5DE735D4A0"
#define IBEACON_UUID3 @"2F1C20BA-1B11-4E71-8FF6-168BC340688C"
#define IBEACON_UUID4 @"65E8D547-2FAE-43B5-9EF7-5D781958E4BB"
#define IBEACON_UUID5 @"C330E0D1-133B-4618-9ACA-EE5611123441"


#define IBEACON_STORE_NAME_1 @"Porsche"
#define IBEACON_STORE_NAME_2 @"BMW"
#define IBEACON_STORE_NAME_3 @"Burberyy"
#define IBEACON_STORE_NAME_4 @"Ferarri"
#define IBEACON_STORE_NAME_5 @"Lamborghini"


//NSString *uuidPatternString = @"^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$";

@interface HomeScreenViewController ()

@end

@implementation HomeScreenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    spotzyLocationManager = [[CLLocationManager alloc] init];
//    spotzyLocationManager.delegate = self;
    
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:STORE_MANAGER_KEY];
    storeManager = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    if (!storeManager) {
        storeManager = [StoreManager sharedStoreManager];
        
        if (storeManager.allStores.count <= 0) {
            Store *s1 = [[Store alloc] initWithName:IBEACON_STORE_NAME_1 andID:0 uuidString:IBEACON_UUID];
            [s1 addBranchWithBranchName:@"Tagamo3" andMajorValue:1];
            [s1 addDepartmentWithDepartmentName:@"Boxster" andMinorValue:0];
            [s1 addDepartmentWithDepartmentName:@"911 Carerra" andMinorValue:1];
            
            Store *s2 = [[Store alloc] initWithName:IBEACON_STORE_NAME_2 andID:1 uuidString:IBEACON_UUID2];
            Store *s3 = [[Store alloc] initWithName:IBEACON_STORE_NAME_3 andID:2 uuidString:IBEACON_UUID3];
            Store *s4 = [[Store alloc] initWithName:IBEACON_STORE_NAME_4 andID:3 uuidString:IBEACON_UUID4];
            Store *s5 = [[Store alloc] initWithName:IBEACON_STORE_NAME_5 andID:4 uuidString:IBEACON_UUID5];
            
            [storeManager.allStores addObject:s1];
            [storeManager.allStores addObject:s2];
            [storeManager.allStores addObject:s3];
            [storeManager.allStores addObject:s4];
            [storeManager.allStores addObject:s5];
            
//            [[NSUserDefaults standardUserDefaults] setObject:s1 forKey:STORE_MANAGER_KEY];
        }
    }
    
    [storeManager saveToDefaults];
    
    storeManager.delegate = self;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Location manager delegate


- (void)locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(CLRegion *)region withError:(NSError *)error {
    NSLog(@"Failed monitoring region: %@", error);
}


- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"Location manager failed: %@", error);
}

- (void) locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region {
    
    NSLog(@"Did enter region");
    
    self.ibeaconStatusTextField.text = @"In range";
}


- (void) locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region {
    
    NSLog(@"Did exit region");
    
    self.ibeaconStatusTextField.text = @"Out of range";
    
}

- (void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region {
    
    if ([beacons count] > 0) {
        CLBeacon *nearestExhibit = [beacons firstObject];
        
        for (Store *store in storeManager.subscribedStores) {
            if ([store.storeUUIDString isEqualToString:nearestExhibit.proximityUUID.UUIDString]) {
                self.storeNameLabel.text = store.storeName;
                break;
            }
        }
        
        // Present the exhibit-specific UI only when
        // the user is relatively close to the exhibit.
//        if (CLProximityNear == nearestExhibit.proximity) {
//            [self presentExhibitInfoWithMajorValue:nearestExhibit.major.integerValue];
            
//            NSLog(@"Nearest beacon%@", nearestExhibit);
        
            self.ibeaconStatusRangeTextField.text = [NSString stringWithFormat:@"%@ +/- %.2fm", [self nameForProximity:nearestExhibit.proximity], nearestExhibit.accuracy];
            self.ibeaconUDIDTextField.text = [nearestExhibit.proximityUUID UUIDString];
            self.ibeaconMajorTextField.text = [nearestExhibit.major stringValue];
            self.ibeaconMinorTextField.text = [nearestExhibit.minor stringValue];
        
        if (CLProximityNear == nearestExhibit.proximity) {
        
//            [self fireLocalNotificationWithUUID:[nearestExhibit.proximityUUID UUIDString] andMajor:nearestExhibit.major andMinor:nearestExhibit.minor];
        }
        
            
//        }
//        
//        else {
////            [self dismissExhibitInfo];
//        }
    }
}


#pragma mark - Helper methods

- (NSString *)nameForProximity:(CLProximity)proximity {
    
    switch (proximity) {
        case CLProximityUnknown:
            return @"Unknown";
            break;
            
        case CLProximityImmediate:
            return @"Immediate";
            break;
            
        case CLProximityNear:
            return @"Near";
            break;
            
        case CLProximityFar:
            return @"Far";
            break;
    }
}



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    NSLog(@"Segue");
    
    if ([segue.identifier isEqualToString:@"PresentStoreManager"]) {
//        [(StoreManagerViewController *) segue.destinationViewController setDelegate:self];
        
        UINavigationController *navVC = segue.destinationViewController;
        
        [(StoreManagerViewController *)navVC.topViewController setDelegate:self];

    }
    
    
}


#pragma mark - StoreManagerVC delegate

- (void)storeManagerVCDidFinish:(StoreManagerViewController *)storeManagerVC {
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    [storeManager saveToDefaults];
    
}

@end








