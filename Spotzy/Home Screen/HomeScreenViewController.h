//
//  HomeScreenViewController.h
//  Spotzy
//
//  Created by Mohamed Nassar on 29/11/2014.
//  Copyright (c) 2014 Mohamed Nassar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "StoreManagerViewController.h"

//@class StoreManager;


@interface HomeScreenViewController : UIViewController <CLLocationManagerDelegate, StoreManagerViewControllerDelegate> {
    
    CLLocationManager *spotzyLocationManager;
    CLBeaconRegion *beaconRegion;
    
    StoreManager *storeManager;
    
}

@property (weak, nonatomic) IBOutlet UILabel *ibeaconStatusTextField;
@property (weak, nonatomic) IBOutlet UILabel *ibeaconStatusRangeTextField;
@property (weak, nonatomic) IBOutlet UILabel *ibeaconMajorTextField;
@property (weak, nonatomic) IBOutlet UILabel *ibeaconMinorTextField;
@property (weak, nonatomic) IBOutlet UILabel *ibeaconUDIDTextField;
@property (weak, nonatomic) IBOutlet UILabel *storeNameLabel;


@end
