//
//  AppDelegate.m
//  Spotzy
//
//  Created by Mohamed Nassar on 26/11/2014.
//  Copyright (c) 2014 Mohamed Nassar. All rights reserved.
//

#import "AppDelegate.h"
#import "AdViewController.h"
#import "StoreManager.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    
    //-- Set Notification
    if ([application respondsToSelector:@selector(isRegisteredForRemoteNotifications)])
    {
        // iOS 8 Notifications
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        
        [application registerForRemoteNotifications];
    }
    else
    {
        // iOS < 8 Notifications
        [application registerForRemoteNotificationTypes:
         (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];
    }
    
    NSDictionary *notificationPayload = launchOptions[UIApplicationLaunchOptionsRemoteNotificationKey];
    
    NSLog(@"%@", notificationPayload);
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    
    NSLog(@"Local notification: %@", notification);
    
    AdViewController *adViewController = [[AdViewController alloc] init];
    
    StoreManager *storeManager = [StoreManager sharedStoreManager];
    Store *selectedStore;
    
    for (Store *store in storeManager.subscribedStores) {
        if (store.storeID == [[notification.userInfo objectForKey:STORE_ID] intValue]) {
            selectedStore = store;
        }
    }
    
    [self.window.rootViewController presentViewController:adViewController animated:YES completion:NULL];
    adViewController.adTextView.text = [NSString stringWithFormat:@"Special offer from %@\n\nClaim it now!", selectedStore.storeName];
    adViewController.adTextView.font = [UIFont systemFontOfSize:16];
    
    
    if (![selectedStore.storeName isEqualToString:@"Burberry"]) {
        adViewController.adMainImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@.jpg", selectedStore.storeName]];
    }
    
    else {
        adViewController.demoImageView.hidden = NO;
    }
}

@end
