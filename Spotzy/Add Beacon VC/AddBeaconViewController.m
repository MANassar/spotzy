//
//  AddBeaconViewController.m
//  Spotzy
//
//  Created by Mohamed Nassar on 06/12/2014.
//  Copyright (c) 2014 Mohamed Nassar. All rights reserved.
//

#import "AddBeaconViewController.h"
#import <CoreLocation/CoreLocation.h>

@interface AddBeaconViewController ()

@end

@implementation AddBeaconViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    FXFormController *formController = [[FXFormController alloc] init];
//    formController.form = [[AddBeaconForm alloc] init];
    
//    self.view addSubview: formController
    
//    self.formController = [[FXFormController alloc] init]
//    self.formController.form = [[AddBeaconForm alloc] init];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
//    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)saveBeaconButtonTapped:(UIBarButtonItem *)sender {
    
    CLLocationManager *locationManager = [[CLLocationManager alloc] init];
    
    
}

- (IBAction)cancelButtonTapped:(UIBarButtonItem *)sender {
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    
}


#pragma mark - Helper methods

-(BOOL)isValidUUID : (NSString *)UUIDString
{
    NSUUID* UUID = [[NSUUID alloc] initWithUUIDString:UUIDString];
    if(UUID)
        return true;
    else
        return false;
}


#pragma mark - UITextFieldDelegate

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    if (textField == self.beaconUUIDTextField) {
        if (![self isValidUUID:textField.text]) {
            textField.textColor = [UIColor redColor];
            self.saveBarButtonItem.enabled = NO;
        }
        
        else {
            textField.textColor = [UIColor blackColor];
            self.saveBarButtonItem.enabled = YES;
        }
    } //end UUID text field
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField == _beaconUUIDTextField) {
        [_beaconUUIDTextField resignFirstResponder];
        [_beaconMajorTextField becomeFirstResponder];
    }
    
    else if (textField == _beaconMajorTextField) {
        [_beaconMajorTextField resignFirstResponder];
        [_beaconMinorTextField becomeFirstResponder];
    }
    
    return YES;
}

@end
