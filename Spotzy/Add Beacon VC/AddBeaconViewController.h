//
//  AddBeaconViewController.h
//  Spotzy
//
//  Created by Mohamed Nassar on 06/12/2014.
//  Copyright (c) 2014 Mohamed Nassar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FXForms/FXForms.h>
#import "AddBeaconForm.h"

@interface AddBeaconViewController : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *beaconUUIDTextField;
@property (weak, nonatomic) IBOutlet UITextField *beaconMajorTextField;
@property (weak, nonatomic) IBOutlet UITextField *beaconMinorTextField;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveBarButtonItem;

- (IBAction)saveBeaconButtonTapped:(UIBarButtonItem *)sender;
- (IBAction)cancelButtonTapped:(UIBarButtonItem *)sender;

@end
