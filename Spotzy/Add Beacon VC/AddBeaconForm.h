//
//  AddBeaconForm.h
//  Spotzy
//
//  Created by Mohamed Nassar on 07/12/2014.
//  Copyright (c) 2014 Mohamed Nassar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FXForms/FXForms.h>

@interface AddBeaconForm : NSObject <FXForm>



@property (nonatomic, copy) NSString *uuid;
@property int major;
@property int minor;

@end
