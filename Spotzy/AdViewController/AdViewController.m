//
//  AdViewController.m
//  Spotzy
//
//  Created by Mohamed Nassar on 02/01/2015.
//  Copyright (c) 2015 Mohamed Nassar. All rights reserved.
//

#import "AdViewController.h"

@interface AdViewController ()

@end

@implementation AdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    originalButtonFrame = self.getOfferButton.frame;
    newButtonFrame = CGRectMake(115, 501, 144, 30);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)getOfferButtonTapped:(UIButton *)sender {
    
    [UIView animateWithDuration:0.5 animations:^{
       
//        self.getOfferButton.frame = newButtonFrame;
        self.getOfferButton.enabled = NO;
        self.getOfferButton.alpha = 0.0;
        self.barcodeImageView.alpha = 1.0;
        
    }];
    
}

- (IBAction)closeButtonTapped:(UIBarButtonItem *)sender {
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    
}
@end
