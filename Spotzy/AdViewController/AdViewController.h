//
//  AdViewController.h
//  Spotzy
//
//  Created by Mohamed Nassar on 02/01/2015.
//  Copyright (c) 2015 Mohamed Nassar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdViewController : UIViewController {
    CGRect originalButtonFrame;
    CGRect newButtonFrame;
}

@property (weak, nonatomic) IBOutlet UIImageView *adMainImageView;
@property (weak, nonatomic) IBOutlet UITextView *adTextView;
@property (weak, nonatomic) IBOutlet UIButton *getOfferButton;
@property (weak, nonatomic) IBOutlet UIImageView *barcodeImageView;
@property (weak, nonatomic) IBOutlet UILabel *storeNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *beforePriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *savePercentLabel;
@property (weak, nonatomic) IBOutlet UILabel *expirationLabel;
@property (weak, nonatomic) IBOutlet UILabel *modifiedPriceLabel;
@property (weak, nonatomic) IBOutlet UIImageView *demoImageView;


- (IBAction)getOfferButtonTapped:(UIButton *)sender;
- (IBAction)closeButtonTapped:(UIBarButtonItem *)sender;

@end
