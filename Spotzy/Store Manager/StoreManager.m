//
//  StoreManager.m
//  Spotzy
//
//  Created by Mohamed Nassar on 11/12/2014.
//  Copyright (c) 2014 Mohamed Nassar. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "StoreManager.h"

@implementation StoreManager

+ (id)sharedStoreManager {
    
    static StoreManager *sharedStoreManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedStoreManager = [[self alloc] init];
    });
    return sharedStoreManager;
}

- (id)init {
    if (self = [super init]) {
        
        self.subscribedStores = [[NSMutableArray alloc] init];
        self.allStores = [[NSMutableArray alloc] init];
        spotzyLocationManager = [[CLLocationManager alloc] init];
        spotzyLocationManager.delegate = self;
    }
    
    return self;
}


- (void)subscribeToStore:(Store *)store {
    
    [self.subscribedStores addObject:store];
    
    NSUUID *beaconUUID = [[NSUUID alloc] initWithUUIDString:store.storeUUIDString];
    CLBeaconRegion *beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:beaconUUID identifier:store.storeName];
    
    
    BOOL canMonitor = [CLLocationManager isMonitoringAvailableForClass:[CLBeaconRegion class]];
    
    CLAuthorizationStatus authStat = [CLLocationManager authorizationStatus];
    
    if (!canMonitor) {
        NSLog(@"Monitoring not available on this device. You probably dont have the needed hardware.");
    }
    
    else {
        
        if (authStat == kCLAuthorizationStatusAuthorized) {
            [self regsiterBeaconRegion:beaconRegion];
            
        }
        
        else {
            [spotzyLocationManager requestAlwaysAuthorization];
        }
        
    }
}


- (void) unsubscribeFromStore:(Store *)store {
    
    [self.subscribedStores removeObject:store];
    
}


#pragma mark - 

- (void) regsiterBeaconRegion:(CLBeaconRegion *)beaconRegionToRegister {
    
    NSLog(@"Registering beacon %@", beaconRegionToRegister);
    
    [spotzyLocationManager startMonitoringForRegion:beaconRegionToRegister];
    [spotzyLocationManager startRangingBeaconsInRegion:beaconRegionToRegister];
    
    
}

- (void)regsiterAllSubscribedStoresBeaconRegions {
    
    BOOL canMonitor = [CLLocationManager isMonitoringAvailableForClass:[CLBeaconRegion class]];
    CLAuthorizationStatus authStat = [CLLocationManager authorizationStatus];
    
    if (!canMonitor) {
        NSLog(@"StoreManager: Monitoring not available on this device. You probably dont have the needed hardware.");
    }// end if
    
    else {
        
        if (authStat == kCLAuthorizationStatusAuthorized) {
            for (Store *store in self.subscribedStores) {
                NSUUID *beaconUUID = [[NSUUID alloc] initWithUUIDString:store.storeUUIDString];
                
                CLBeaconRegion *beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:beaconUUID identifier:store.storeName];
                [self regsiterBeaconRegion:beaconRegion];
                
            } //end for loop
        }//end if
        
        else {
            [spotzyLocationManager requestAlwaysAuthorization];
        }
        
        
    } //end else
    
    
    
}

#pragma mark - Location manager delegate

- (void)locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(CLRegion *)region withError:(NSError *)error {
    NSLog(@"Failed monitoring region: %@", error);
}


- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"Location manager failed: %@", error);
}

- (void) locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    
    if (status == kCLAuthorizationStatusAuthorized || status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [self regsiterAllSubscribedStoresBeaconRegions];
    }
    
}

- (void) locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region {
    
    NSLog(@"Did enter region");
    
    
    
    if ([region isKindOfClass:[CLBeaconRegion class]]) {
        
        CLBeaconRegion *detectedRegion = (CLBeaconRegion *)region;
        
        for (Store *store in self.subscribedStores) {
            if ([store.storeUUIDString isEqualToString:detectedRegion.proximityUUID.UUIDString]) {
                
                [self fireLocalNotificationWithStore:store];
                
                break;
            }
        }
        
    }
    
    
    if ([self.delegate respondsToSelector:@selector(locationManager:didEnterRegion:)]) {
        
        [self.delegate locationManager:manager didEnterRegion:region];
        
    }
    
}


- (void) locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region {
    
    NSLog(@"Did exit region");
    
    
    if ([self.delegate respondsToSelector:@selector(locationManager:didExitRegion:)]) {
        
        [self.delegate locationManager:manager didExitRegion:region];
        
    }
}

- (void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region {
 
    if ([self.delegate respondsToSelector:@selector(locationManager:didRangeBeacons:inRegion:)]) {
        
        [self.delegate locationManager:manager didRangeBeacons:beacons inRegion:region];
        
    }
    
}

//#pragma mark - Helper methods
//
//- (NSString *)nameForProximity:(CLProximity)proximity {
//    
//    switch (proximity) {
//        case CLProximityUnknown:
//            return @"Unknown";
//            break;
//            
//        case CLProximityImmediate:
//            return @"Immediate";
//            break;
//            
//        case CLProximityNear:
//            return @"Near";
//            break;
//            
//        case CLProximityFar:
//            return @"Far";
//            break;
//    }
//}


#pragma mark - Notifications

- (void) fireLocalNotificationWithUUID: (NSString *) beaconUUID andMajor: (NSNumber *) major andMinor: (NSNumber *) minor {
    
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    notification.alertBody = [NSString stringWithFormat:@"Check out our latest offers"];
    notification.soundName = @"Default";
    
    NSDictionary *info = [NSDictionary dictionaryWithObjectsAndKeys:beaconUUID, @"UUID", major, @"Major", minor, @"Minor", nil];
    
    notification.userInfo = info;
    [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
    
    NSLog(@"Fired local notification");
    
}

- (void) fireLocalNotificationWithStore: (Store *) store {
    
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    notification.alertBody = [NSString stringWithFormat:@"Check out the latest offers from %@", store.storeName];
    notification.soundName = @"Default";
    
    NSDictionary *info = [NSDictionary dictionaryWithObjectsAndKeys:store.storeUUIDString, STORE_UUID_STRING_KEY, [NSNumber numberWithInt:store.storeID], STORE_ID, nil];
    
    notification.userInfo = info;
    [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
    
    NSLog(@"Fired local notification");
    
}


#pragma mark - NSCoding

-(id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [StoreManager sharedStoreManager];
    
    if (self) {
        self.subscribedStores = [aDecoder decodeObjectForKey:SUBSCRIBED_STORES_KEY];
        self.allStores = [aDecoder decodeObjectForKey:ALL_STORES_KEY];
        
//        if (self.subscribedStores.count > 0) {
//            [self regsiterAllSubscribedStoresBeaconRegions];
//        }
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.subscribedStores forKey:SUBSCRIBED_STORES_KEY];
    [aCoder encodeObject:self.allStores forKey:ALL_STORES_KEY];
}


- (void) saveToDefaults {
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self];
    
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:STORE_MANAGER_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

@end
