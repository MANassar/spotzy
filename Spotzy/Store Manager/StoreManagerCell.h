//
//  StoreManagerCell.h
//  Spotzy
//
//  Created by Mohamed Nassar on 11/12/2014.
//  Copyright (c) 2014 Mohamed Nassar. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Store;

@interface StoreManagerCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *storeNameLabel;
@property (weak, nonatomic) IBOutlet UISwitch *storeSubscriptionSwitch;
@property (weak, nonatomic) IBOutlet UILabel *storeUUIDStringLabel;

@property (weak) id delegate;
@property (strong, nonatomic) Store *store;

- (IBAction)cellSwitchValueChanged:(UISwitch *)sender;

@end

@protocol StoreManagerCellDelegate <NSObject>

@required

- (void) cellSwitchValueChanged:(UISwitch *)sender forStore:(Store *) store;

@end
