//
//  Store.h
//  Spotzy
//
//  Created by Mohamed Nassar on 10/12/2014.
//  Copyright (c) 2014 Mohamed Nassar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

#define STORE_UUID_STRING_KEY @"storeUUIDString"
#define STORE_NAME_KEY @"storeNameKey"
#define BRANCH_MAJOR_VALUES_KEY @"branchMajorValues"
#define DEPARTMENT_MINOR_VALUES_KEY @"departmentMinorValues"
#define STORE_SUBSCRIPTION_STATUS @"storeSubscriptionStatus"
#define STORE_ID @"storeID"

@interface Store : NSObject <NSCoding>

@property int storeID;
@property (strong, nonatomic) NSString *storeUUIDString;
@property (strong, nonatomic) NSString *storeName;
@property (strong, nonatomic) NSMutableDictionary *branch_MajorValues;
@property (strong, nonatomic) NSMutableDictionary *department_MinorValues;
@property BOOL subscriptionStatus;

- (Store *) initWithName:(NSString *) storeName andID: (int) sid uuidString:(NSString *) uuidString;

- (void) addBranchWithBranchName: (NSString *) branchName andMajorValue: (CLBeaconMajorValue) majorValue;

- (void) addDepartmentWithDepartmentName: (NSString *) deptName andMinorValue: (CLBeaconMinorValue) minorValue;


@end
