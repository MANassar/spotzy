//
//  StoreManager.h
//  Spotzy
//
//  Created by Mohamed Nassar on 11/12/2014.
//  Copyright (c) 2014 Mohamed Nassar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "Store.h"

#define SUBSCRIBED_STORES_KEY @"subscribedStoresKey"
#define ALL_STORES_KEY @"allStores"
#define STORE_MANAGER_KEY @"storeManagerKey"



@interface StoreManager : NSObject <CLLocationManagerDelegate, NSCoding> {
    
    CLLocationManager *spotzyLocationManager;
}

@property (strong, nonatomic) NSMutableArray *subscribedStores;
@property (strong, nonatomic) NSMutableArray *allStores;

@property (weak) id delegate;

+ (id) sharedStoreManager;

- (void) subscribeToStore: (Store *) store;
- (void) unsubscribeFromStore: (Store *) store;
- (void) regsiterAllSubscribedStoresBeaconRegions;
- (void) saveToDefaults;

@end
