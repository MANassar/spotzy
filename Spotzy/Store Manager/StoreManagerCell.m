//
//  StoreManagerCell.m
//  Spotzy
//
//  Created by Mohamed Nassar on 11/12/2014.
//  Copyright (c) 2014 Mohamed Nassar. All rights reserved.
//

#import "StoreManagerCell.h"

@implementation StoreManagerCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)cellSwitchValueChanged:(UISwitch *)sender {
    
    NSLog(@"Value changed in cell %@", self);
    
    [self.delegate cellSwitchValueChanged:sender forStore:self.store];
    
}
@end
