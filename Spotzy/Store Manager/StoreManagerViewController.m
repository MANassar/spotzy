//
//  StoreManagerViewController.m
//  Spotzy
//
//  Created by Mohamed Nassar on 11/12/2014.
//  Copyright (c) 2014 Mohamed Nassar. All rights reserved.
//

#import "StoreManagerViewController.h"
#import "StoreManager.h"

#define IBEACON_UUID @"9C0197EE-1B84-46EF-BC9C-3BFD2767DA05"
#define IBEACON_UUID2 @"B6FF08A0-0AF5-441C-9A13-AD5DE735D4A0"
#define IBEACON_UUID3 @"2F1C20BA-1B11-4E71-8FF6-168BC340688C"
#define IBEACON_UUID4 @"65E8D547-2FAE-43B5-9EF7-5D781958E4BB"
#define IBEACON_UUID5 @"C330E0D1-133B-4618-9ACA-EE5611123441"



@interface StoreManagerViewController ()

@end

@implementation StoreManagerViewController

@synthesize storeManager;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    storeManager = [StoreManager sharedStoreManager];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return storeManager.allStores.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    StoreManagerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"storeManagerCell" forIndexPath:indexPath];
    cell.delegate = self;
    

    Store *currentStore = [storeManager.allStores objectAtIndex:indexPath.row];
    
    cell.store = currentStore;
    cell.storeNameLabel.text = currentStore.storeName;
    cell.storeUUIDStringLabel.text = currentStore.storeUUIDString;
    cell.storeSubscriptionSwitch.on = currentStore.subscriptionStatus;
    
    
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cellSwitchValueChanged:(UISwitch *)sender {
    
    NSLog(@"Value changed in cell %@", sender.superview);
    
    StoreManagerCell *storeManagerCell = (StoreManagerCell *) sender.superview;
    
    NSLog(@"UUID is %@", storeManagerCell.storeUUIDStringLabel.text);
    
}

- (IBAction)doneButtonTapped:(UIBarButtonItem *)sender {
    
    [self.delegate storeManagerVCDidFinish:self];
}

- (IBAction)cancelButtonTapped:(UIBarButtonItem *)sender {
}


#pragma mark - Store Manager Cell Delegate

- (void)cellSwitchValueChanged:(UISwitch *)sender forStore:(Store *)store {
    
    //Check the value of the switch
    
    if (sender.on) {
        [storeManager subscribeToStore:store];
        store.subscriptionStatus = YES;
    }
    
    else {
        [storeManager unsubscribeFromStore:store];
        store.subscriptionStatus = NO;
    }
    
}


@end
