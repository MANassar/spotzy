//
//  Store.m
//  Spotzy
//
//  Created by Mohamed Nassar on 10/12/2014.
//  Copyright (c) 2014 Mohamed Nassar. All rights reserved.
//

#import "Store.h"

@implementation Store

- (Store *) initWithName:(NSString *) storeName andID: (int) sid uuidString:(NSString *) uuidString {
    
    self = [super init];
    
    if (self) {
        
        self.storeID = sid;
        self.storeUUIDString = uuidString;
        self.storeName = storeName;
        self.branch_MajorValues = [[NSMutableDictionary alloc] init];
        self.department_MinorValues = [[NSMutableDictionary alloc] init];
        self.subscriptionStatus = NO;
    }
    
    return self;
}

- (void) addBranchWithBranchName: (NSString *) branchName andMajorValue: (CLBeaconMajorValue) majorValue {
    
    [self.branch_MajorValues setObject:branchName forKey:[NSNumber numberWithInteger:majorValue]];
}

- (void) addDepartmentWithDepartmentName: (NSString *) deptName andMinorValue: (CLBeaconMinorValue) minorValue {
    
    [self.department_MinorValues setObject:deptName forKey:[NSNumber numberWithInteger:minorValue]];
    
}

-(id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super init];
    
    if (self) {
        self.storeID = [aDecoder decodeIntForKey:STORE_ID];
        self.storeUUIDString = [aDecoder decodeObjectForKey:STORE_UUID_STRING_KEY];
        self.storeName = [aDecoder decodeObjectForKey:STORE_NAME_KEY];
        self.branch_MajorValues = [aDecoder decodeObjectForKey:BRANCH_MAJOR_VALUES_KEY];
        self.department_MinorValues = [aDecoder decodeObjectForKey:DEPARTMENT_MINOR_VALUES_KEY];
        self.subscriptionStatus = [aDecoder decodeBoolForKey:STORE_SUBSCRIPTION_STATUS];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeInt:self.storeID forKey:STORE_ID];
    [aCoder encodeObject:self.storeName forKey:STORE_NAME_KEY];
    [aCoder encodeObject:self.storeUUIDString forKey:STORE_UUID_STRING_KEY];
    [aCoder encodeObject:self.branch_MajorValues forKey:BRANCH_MAJOR_VALUES_KEY];
    [aCoder encodeObject:self.department_MinorValues forKey:DEPARTMENT_MINOR_VALUES_KEY];
    [aCoder encodeBool:self.subscriptionStatus forKey:STORE_SUBSCRIPTION_STATUS];
}

@end
