//
//  StoreManagerViewController.h
//  Spotzy
//
//  Created by Mohamed Nassar on 11/12/2014.
//  Copyright (c) 2014 Mohamed Nassar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StoreManagerCell.h"

@class StoreManager;

@interface StoreManagerViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, StoreManagerCellDelegate>

@property (strong, nonatomic) StoreManager *storeManager;
@property (weak) id delegate;

- (IBAction)cellSwitchValueChanged:(UISwitch *)sender;
- (IBAction)doneButtonTapped:(UIBarButtonItem *)sender;
- (IBAction)cancelButtonTapped:(UIBarButtonItem *)sender;

@end


@protocol StoreManagerViewControllerDelegate <NSObject>

@required

- (void) storeManagerVCDidFinish: (StoreManagerViewController *) storeManagerVC;

@optional

- (void) storeManagerVCDidCancel: (StoreManagerViewController *) storeManagerVC;
@end